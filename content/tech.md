### TECHNICAL WRITING

> The Linux philosophy is 'Laugh in the face of danger'. Oops. Wrong one. 'Do it yourself'. Yes, that's it. - Linus Torvalds

[Technical Writing](#tech) | [My Stack](#stack) | [Git](#git) | [Deemed Harmful](#harmful) | [Why](#why)

<a id = "tech"></a>

#### Technical Writing

1. [Gaman](/gaman)
1. [Sent from the Command Line](/deliberate)
1. [Do You Want that Word to be 'Like'?](/like)
1. [Why x Tech, Not y Tech?](/whyxnoty)
1. [tmux](/tmux)
1. [There is No Ghost in My Machine](/revolution)
1. [10 Tools to Gaman and Learn](/tentools)
1. [Arch](/arch)
1. [How and Why Use IRC](/irc)

<a id = "stack"> </a> 

#### Stack

+ [Mutt](/deliberate): mail client
+ [Arch](/arch): operating system
+ Terminology: terminal emulator
+ [IRC](/irc) & [tmux](/tmux): digital communications
+ dwm: tiling window manager
+ systemd: system/service manager
+ Node.js
+ Git: distributed version control system
+ Vim: text editor
+ zsh: shell
+ Gitboria, [Gitlab](http://gitlab.com/u/gwenbell): repo management
+ mplayer, alsamixer: music 
+ Gimp, Inkscape: graphic editors
+ Acer Aspire S3: hardware
+ [Digital Ocean](https://www.digitalocean.com/?refcode=14a7846491d4) (rewards link): VPS
+ [NameCheap](http://www.namecheap.com/?aff=56155) (rewards link): domain registrar

<a id = "git"> </a> 

#### Git

I thought Git sounded like a pain in the ass to learn. Indeed, it was. 

But since I locked myself in a basement and learned to use version control, I can't remember a time when I didn't. But it's a little like when I owned a yoga studio and Patrick would say to me, "you don't understand the pain of being overweight. You have no idea what it's like to be fat, because you've never been fat." 

He was right. But this time I _do_ remember the pain of FTP and a lack of version control.

With Git

1. My work is cryptographically secure
1. I can always go back in time to a previous hash (which is good, because I tend to over-delete)
1. I can lose everything - my laptop, site hosting, everything, and still have a snapshot of my work
1. I don't save to 'the cloud' which, as we know, is a bunch of bullshit anyway

#### <a id = "harmful"></a> Deemed Harmful

Twxttr: You could try [IRC](/irc) instead. I'm in #gaman and #bitters on Freenode

xphone: I've lived without a mobile device since January 2012, when I sold the device in The Mission in SF. As of December 2013, I have [incontrovertible proof](http://www.washingtonpost.com/world/national-security/nsa-tracking-cellphone-locations-worldwide-snowden-documents-show/2013/12/04/5492873a-5cf2-11e3-bc56-c6ca94801fac_print.html) that was a good decision.

Fxxxbook: Distracting to the point of dangerous; I've been [clean since December 14, 2010](/like).

Gxxgle products: I [quit all Gxxgle products](http://gwenbell.com/write#ditchpoogle) in 2012 after going to Kansas City to watch the rollout of Fiber. [Gxxgle's algorithms discriminate against women](http://www.theguardian.com/commentisfree/2013/oct/22/google-autocomplete-un-women-ad-discrimination-algorithms). They can ['suggest'](http://www.engadget.com/2012/03/26/japanese-court-orders-google-to-halt-instant-search-for-suggesti/) you out of a job.

#### <a id = "why"></a> Why I Do this Work

While I advocate digital sabbaticals -- taking time away from the machine -- I'm in tech for the long haul. Each time I see a way to upgrade my stack and therefore work, I make it. 

Each time I see a way to pass that information back to my readers, whether in free articles or paid products, I do it. My work is proof you can make the switch to open/free software _and_ make a living doing what you love. If you're looking for technomemoir, it's [here](/write).
