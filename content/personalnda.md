### The Personal NDA

I no longer disclose my location. 

The only person who knows my exact address is [Nunu](http://nunubell.com). When you email and I don't answer the question, here's why.

#### Contracts

As a rule I'm [terrible at contracts](/lesswrong). I sign them as infrequently as possible. But I can keep a contract with myself. And the contract right now is a Personal NDA.

An NDA is a non-disclosure agreement. I used to sign them (and I kept my word) when I did a lot of consulting. You sign them to say you won't talk about what your client is telling you to other people.

My Personal NDA is a form of self-protection. I observed while I was in my home country that if you're [flourishing](/flourish) you're at risk. If you
look as though you're having a good time the haters come out - in person as well as digital, hell-bent on destroying your reputation.

Of course, you can rise above it. But I am better at swimming long distances than paddling hard to keep my head above water.

#### Privacy

I don't want you to know my exact location because I value privacy. If 2013 has taught us anything it's what we thought were private conversations, what we thought were private discussions, were not. 

I had a gut on that in 2012. I was in SF at the time. I was surrounded by iPhones. People had them out at bars, and though I'd sold mine in January of 2012, I was still suspicious that my conversations could be recorded by all the phones around me.

I would not be [the first paranoid writer](http://en.wikipedia.org/wiki/Ernest_Hemingway#Idaho_and_suicide). Though I'd sold my phone, I felt monitored.

#### Freedom

Real freedom, to me, is a combination of privacy and mobility. Prior to the Snowden revelations, I had suspicions they were coming. I was running in Bitcoin circles and those kids trend futuristic. (Did, anyway. Now that it's mainstream, I'm out of it, and I don't track what's going on in there anymore.) 

Anyway, freedom's a need. And so is privacy. 

It's why I run [Arch Linux](/arch) and use Git: freedom and privacy.

#### Healthier, Calmer, More Productive

So I moved abroad. I'm not going to tell you where to. I'm not going to tell you my address. But I will tell you I'm healthier, calmer, and at deep ease with my decision. I have no intention of returning to the mother country until she sorts herself out.

I am aware that could be a very long time. Until then, personal NDAs in full effect.

Do you have a personal NDA? Talk to me about it in [#gaman](/gaman), [freenode](/irc). I have [tmux](/tmux) running.

<div style="font-size: .6em">Published 8 December 2013 | Typo? [Pull request.](http://gitlab.com/gwenbell/gwenbell) </div>
