### The Worst Day to Die

It's November 30th. 

My mom died. She died on the worst day. She died a week after Thanksgiving and three weeks before Christmas. Those are the two biggest holidays in the United States. Thanksgiving, when Americans celebrate the slaughter of the natives. And Christmas, when Americans celebrate the coming again of the lord almighty thank hallelujah.

She died upstairs in Nunu and Bub's bedroom. Only Bub sleeps in there now. I wonder if mom ever visits him and cracks jokes with him. She still visits me. Though never when I'm expecting her (how do you expect someone like that?) and always with some kind of message. This year she sent me the Get Out of the Country message which came as a blue, pulsing light.

I got out of the country.

No, I don't think it's weird that I sometimes get messages from her. I think it's the least she can do. Throw a little light on occasion.

Next year she will have been dead twice as long as she was in my life. 

One time, when I was in high school, this lanky guy whose name I won't mention but I remember it (Maurice), said to me, as I was going into the band room for band practice (I played the flute and later the piccolo and later the baritone because 'you can't hear any of that piccolo at half-time') he said to me, "your mama..." and I braced for it. He said, "your mama is so dead bugs are eating on her right now." 

I remember his spindly fingers, Maurice. 

I turned him into a rock. (Of course, this is a slight exaggeration.)

So, he was a rock and I went home and I let Nunu see me cry. She said something stabilizing about how kids can be such meanies and in the end I would win against those meanies, "don't let it get to you," and I did not, and I did.

#### The Meanie Was Right

My mom was food for worms. She'd been dead a handful of years at that point. And because she was buried, not cremated, she went to the bugs that do the eating on the people who've gone into the earth.

I did a meditation. I might have read about it in one of my favorite books, Japanese Death Poems. I went to the campus forest amphitheatre and imagined mom's body going to the worms. Them eating it bit by bit. I cried, hand-wrote a letter to Della Pollock who is one of the people who taught me to tell stories good and proper, walked to her office, slid it under her door and walked away.

I was not over her, but I'd imagined her body going to the worms.

#### Six Feet Under

The final nail in the coffine came after traveling the globe, recuperating after a marriage and [divorce](/writing) I got in in time to hit the age mom was when she died. I moved into Nunu and Bub's basement. After years of not going back to them proper, I sought refuge. 

I taught myself to program, brought all my [tech skills up](/tech), and watched every episode of Six Feet Under.

I cried so hard, so totally, for the season finale. 

Then, I went to New York City. I wrote Terminal which hardly nobody bought but I didn't care because mom was in it in it (I can't remember that book at all by the way, so those of you who _did_ buy it, thank you. You carry something in you even I've let go). 

And like that, I healed over.

#### How to Die Well

I have never gotten over her death. But I have healed and now I don't make the [mistakes](/lesswrong) I've made before because I know now, at all times, what's up for me. What nutrients I'm missing, how to get them.

And I can't say I'd want it any other way, but I will say this. Now that I contemplate my own death daily, my mom taught me the most important lesson: how to die well.

***

Maybe your end of year is hard, too. [Gaman](/gaman).

**Further reading**

+ [Japanese Death Poems](https://en.wikipedia.org/wiki/Death_poem) and [my favorite book on the subject](http://www.alibris.com/Japanese-Death-Poems-Written-by-Zen-Monks-and-Haiku-Poets-on-the-Verge-of-Death-Zen-Monks/book/10623407)
+ [How Doctors Die](http://www.zocalopublicsquare.org/2011/11/30/how-doctors-die/ideas/nexus/)
+ [Della Pollock](http://journal.oraltradition.org/authors/show/249)
