<a href="http://gwenbell.fetchapp.com/sell/aihaneer"><img src="/images/alignyourwebsite.jpg" class="profile" style="width: 40%; float: right; margin-left: 1em; margin-bottom: 1em;" ></a>

### Gwen Bell

Hi, I'm [Gwen](/about). I'm an [expat](/expat) [technical](/tech) [writer](/write).

9 December 2013 | [The Personal NDA](/personalnda)

8 December 2013 | [Flourish](/flourish), an edited [gaman](/gaman) response. Merging requests, [gaman](/gaman) peeps. Then I gotta write!

7 December 2013 | Updated [appreciations with When Society...](/appreciations). Merged #gaman requests. Enjoy reading with pseudo-anonymity (initials rather than full names.) 

6 December 2013 | gpom'd gb, cy, gp for [gaman](/gaman)

5 December 2013 | [Merge requests](http://gitlab.com/gwenbell/gaman), welcoming newcomers and reading entries for [#gaman](/gaman).

4 December 2013 | Read a review from the folks at [Braid Creative](http://www.braidcreative.com/blog/website-alignment-with-gwen-bell). Then, [do it](http://align.gwenbell.com).



