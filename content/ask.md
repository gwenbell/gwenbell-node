### ASK

**Updated Sun Nov 17 18:10:37 PST 2013**

Gaman it is. Details soon.

<hr />

Full inbox this morning. Y'all have been sending suggestions (and the response has been all excitement, which delights me) in response to the ask letter. 

These are your suggestions so far: #letgo, #givingtaking ("inspired by Adam Grant's book, Give and Take"), #pineconemagic, #missionbell, #bare ("As in, let's bare our souls! Our failings! Our grumpy egos! Let's tell the truth and be vulnerable."), #unwrappingtheyear, #bestandworstof2013, #gaman (as "an encouragement of soulful reflection and work to be done offline" -- gaman so far has the most votes), #cleanslate, #release, #commit, #unravel. 

Keep the suggestions coming, via email or in #bitters on IRC.

<hr />

It's coming up on the end of the year. I bet you feel it, too. 

We're doing this end of year thing again, for the first time in two years. Here's what I want to know: what do we call this end of year thing we do? This year, for 2013?

#### PREAMBLE: BEST 09

I've always hated the end of the year for all manner of reasons, top of which is it's a reminder of the day my mom died (the last day of November) and the shittiness of holidays without her. In 2009, because I wanted to have a different kind of end to the year, more celebration less sadness, I decided to take matters into my own hands. Along with Brandon White, who at the time co-owned my favorite cafe (which, I just duckduckgo'd and it turns out opened in 2009 and closed this year - a four year run), I brainstormed 31 prompts. One for each day in December.

I gave it a hashtag, #best09, and put up a link list on this site. I didn't expect anyone to respond to the prompts but me, and then suddenly, people _did_. 

It was crazy. A few at first, then hundreds.

It was a little stressful, but overall, kept me from feeling super sad as the year wound down.

#### REDUX: REVERB 10

The next year, along with Cali Gater and Kaileen Elise, we went to Napa to Kaileen's grandparent's house, and ran it again, this time as Reverb10.

But something happened during #reverb10 (the second manifestation of #best09). I met someone at Yerba Buena park, mid-way through December 2010. He was wearing a funny hat and Doctor Whoish scarf, and he knew more about e-books than me. It seemed to me, however sappy as shit this sounds, I'd met my star twin.

That was the end of the year, 2010. During #reverb10 I felt the stirrings of love. 

I was married, though already living on my own (we were living together, apart) and I decided it probably best to not answer the prompts in a frank manner in full view of the public. Buster'd created 750words and I journalled some there, and offline a ton. But for the most part, I dropped off the radar at the end of 2010.

Then, the start of 2011. While still married, my two person relationship forked to include a third, and for a bit the three of us were in relationship together. 

I guess that sort of setup's becoming the rage right now. Perhaps I was a little too ahead of the curve on that one. But anyway! It went off the tracks at some point. In 2011 and 2012, though people got in touch to ask if I'd run Reverb or Best09 or something like it, I didn't. I wasn't able to hold several hundred, let alone thousand, people in my mind in 2011 and 2012. What I haven't talked about in public is how it ended. That's because police and courts were involved, and it wasn't, and still isn't, something I want to put on the record. 

Still!

Still, I say. We always need places to express ourselves. It helps to know there are others doing the work of self-exploration. Because self-exploration, or knowing yourself, is a step toward self-reliance.

#### POST-NSA

Since the NSA breaking news, it makes sense you wouldn't want to pour your whole heart onto the web anymore. I sure as shit don't want to pour mine out for the NSA to log.

Privacy is top of mind. A breach of trust takes time to recover from, and in the meantime, we can journal to our notebooks, or [speak in code](/moretruth). But speak, write, create, kvetch, and otherwise get it out we must, because otherwise, those who breach trust win.

Whatever this year's end of year project is, I hope that you keep at least part, if not all, of it off the public web. I just don't think you can do the depth of work and feel the feels of feelings you must sometimes, to heal, when publishing it to a giant database somewhere in Utah.

#### THE ASK

What is this thing we do? What is this end of year project? 

It's certainly not #best anything anymore, nor is it #reverb, a call to reverberate. It's more reflection than anything, but also it's a chance to connect. 

It's knowing there are others with you. Maybe it's best+worst? Maybe it's a Kvetch Fest (although, I'm not a huge kvetcher). Maybe it's one month of creative destruction, tearing out the binding, so we can work through what 2012 was and then forget it ever happened. Is it #miserablegit?

What do you think?

[Email](mailto:gwen@gwenbell.com), or find me on IRC in #bitters (Freenode) for a real-time chat (if I miss you'll, I'll still get your message, thanks to tmux. And if you don't know IRC, I'll bring you up to speed before the year's end. This year, you'll need it, as that'll be our hub.

I'll leave you with this from _1984_,

> He did not feel any temptation to tell lies to her. It was even a sort of love offering to start off by telling the worst.

Perhaps this year, we start there, in the most honest place, with the worst. #worst13? 

I'm asking because I'm open to ideas on this one. Holler back. Even if it's just a couple of us, let's wrap this year together. It's the least we can do.

-gb
