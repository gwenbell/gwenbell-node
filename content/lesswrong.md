### MISTAKES

> <@LeoTal> gwenbell: Biggest lesson learnt from a mistake

Biggest lesson learned from a mistake? That's what LeoTal asked when I popped into [gwern's IRC channel](http://gwern.net).

How do I course correct? That's what this page is for.

#### The Trouble with Mistakes Were Made

"Mistakes were made." I hate that phrase. Adds insult to injuries. Not "I fucked up" but the intentional passive tense..."mistakes were made". I heard that so much in 2013, but [most and worst and shittiest of all from the tech world](https://news.ycombinator.com/item?id=6873703). "Mistakes were made" to let themselves off the hook, rather than
saying "this company, while I was at the helm" did x.

That frustration and LeoTal's question birthed this mistakes page. Not to focus on fucking up. But to acknowledge I have fucked up. And how I have or am course correcting. Not just a log for myself, but a log for the world. Because I don't see that enough, "I FUCKED UP." Almost everyone writing in [gaman](/gaman) can see how people in their lives fucked up...but not the ways in which they've fucked up.

It's fascinating. Less fascinating, more telling.

I Fuck(ed) Up.

That is why I've published the mistakes page.

#### Singapore Shrimp

<mark>Lesson Learned: Stay Out of Singapore</mark>

I will never return to Singapore. And if I were to return to Singapore, I would not eat the shrimp. But I will not return to Singapore. 

[William Gibson](http://www.wired.com/wired/archive/1.04/gibson.html) got it right in his first major piece of non-fiction. 

It's Disneyland with the death penalty. Of course, you can't say that there.

In the second, final month of my stay, I ate a Vietnamese Spring roll. It consisted of fresh cilantro, shrimp that had been cooked and then refrigerated perhaps, lettuce in a rice wrap.

It is the sickest I've ever gotten off a dish. It was diarrhea-vomit (finally. I should have puked right away but didn't. Because I hate puking). 

I was down for a week. I thought I was going to die in Singapore. I did not die. I _wanted_ to die. I held very still and made peace with my impending death. 

I told nobody besides EB. I figured I'd be quarantined and never heard from again. (Something I learned from the cruise I took one time. The all expenses paid, and it was the most well known cruise line in the world, free trip. After that I swore I would never take a cruise again.  The old guard told me not to make a peep when you're sick; they'll quarantine your ass. Take note cruise ship wanna-goers. Read DFW's piece A Supposedly Fun Thing... for more on the subject.)

Back in Singapore, I am finally puking and I swear I will never go back to Singapore, and that I'll never eat shrimp again. E coli. Please never again.

#### Mawwaige

<mark>Lesson Learned: Stay with Gut; Don't Sign Long-term Contracts</mark>

I've written [a thing](http://gwenbell.com/write#divorce) about getting divorced. I should have stuck to my guns on the marriage front. [In 2007 - thank you Wayback - I wrote about how I wanted a micro-marriage](http://web.archive.org/web/20071012221738/http://www.gwenbell.com/blog/2007/10/07/micro-marriage-a-new-kind-of-proposal/). A marriage that both parties agreed to for a set period of time.* 

In the end, though, I wanted to be part of a family I thought was pretty cool. And I loved the guy. So, we went for it. 

(Btdubs, the need I was attempting to meet, in NVCspeak is 'belonging'. Now that I know what I'm needing at all times, I don't have to make so many long-term commitments. In fact, I can make zero. Speaking of, check my [appreciations](/appreciations) page for ANVC, which is, to my mind, a more honest approach to NVC.)

Turns out, I am not wife material. 

I even got a hedgehog to try to settle down. But I was terrible at it, the whole fucking thing. I felt trapped, I gave myself Bells Palsy for a couple of weeks. I didn't buy the family thing the way I needed to to do it well. 

That, and I prefer other things to settling down. Learning, traveling, pushing myself, pushing my readers, pushing my clients and having minor breakthroughs on occasion. If you've read Steven Pressfield's _The War of Art_ you will understand when I say marriage was, for me, The Ultimate Resistance.

I course corrected by ending it within two years.

#### Geoarbitrage Only Works When You're Hustlin' in the Right Direction

<mark>Lesson Learned: If You're Going Broke, Go Somewhere Cheaper</mark>

Earlier this year in Bed-Stuy I saw a guy wearing a hoodie that said, "You're huslin' in the wrong direction," and of course because I was going the opposite direction he was, he must have been hustlin' in the _right_ direction.

So! Geoarbitrage. Only works when you hustle in the right direction. Going broke? Don't bounce from Singapore to Japan to Berlin. Don't go to Singapore in the first place, but where should you go instead? If you earn in USD, go somewhere the dollar is stronger than the local currency. That's Geoarbitrage 101.

If you fail Geoarbitrage 101 as a location-free person, you fail. You hustle in the wrong direction, you fail.

It's fine. You wind up in a basement where you learn harder tech skills. Git, Node.js, Bitcoin, cryptography, whatever. In my case, I also watched every episode of Six Feet Under, which I never would have done had I _not_ been in the basement. But yeah, doesn't hurt to occasionally pop your head out of the sand and check which direction you're hustlin'.

#### Thinking I'd be Dead by 30

<mark>Lesson Learned: Beliefs Shape Reality</mark>

Thinking I'd die by 30 and panicking about my health on a regular basis until I hit 30. Funny to me now. WAS NOT funny to me prior to thirty. But I was totally sure I'd be dead at the same age mom died, right. Because if your mom dies, you must have to die, too. 

Some of my other missteps connect to this one. I told the person I married, and then divorced, that I would have wanted to have a partner at my side when I died...at thirty. Not what you tell someone you've divorced. 

So, I guess I got married because I wanted to get it in there before I died; I thought my death was imminent. And I wanted to belong and he had/has a massive/amazing fam.

But I didn't die. I'm very much alive. _Waylive_.

What's another thing I wish I'd known?

#### Not Knowing About Quantitative Easing

<mark>Lesson Learned: The US Government's Printing Money, Respond Accordingly</mark>

I wish I'd known about that in 2009 when I got married. Because they'd just started that bullshit back then. And perhaps had I known I would have said, "hmm...there's a reason people are so flush right now. The gov's printing money!!!" But I didn't know about it. And now that I do, I see things could be a lot worse than they are.

The other thing about not dying when I thought I would is this: I did everything on my "bucket-do-before-you-die list" and now I'm ancient. I mean, I did everything I wanted to do, and that made me at least 1,000 years old. Then I hit thirty and I (I just shrugged) well, it's all pruning from here til death, I reckon. 

Nothing else major on the life list. Which let's me off the hook to really get into the heart pulsing moment.

Which. Yes.

Oh! And!

#### The Vegetarian Thing

<mark>Lesson Learned: Meat Stabilizes</mark>

I wouldn't have been so veggie so long. I was pretty much hungry all of my twenties. Emotional crashes each day. In the decade plus I was vegetarian I never held it against meat eaters. I just wanted to do the do no harm to animals thing. Though, I'm an animal, too. And I was doing harm to myself.

That said, I have no plans to go the [raw meat mono diet](http://www.vice.com/en_uk/read/this-guy-has-eaten-nothing-but-raw-meat-for-five-years). [Why?](http://www.theguardian.com/lifeandstyle/2013/oct/02/raw-meat-mono-diet-nutritionists-comment)

***

How about you? Biggest lesson learned from a mistake? How are you learning to be less wrong? I'm in IRC: [#gaman](/gaman).

*[Mexico proposed micro-marriage](http://www.mexiconews.net/story/853830) a few years after I published the piece

<div style="font-size: .6em">Published 27 November 2013, updated 8 December 2013. Typo? [Pull request](http://gitlab.com/gwenbell/gwenbell).</div>

