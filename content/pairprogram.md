### Pair Programming && Pair Publishing

I've gotten so many requests from writers who want to learn to program, and from programmers who want to learn to write, I decided to offer pair programming and pair publishing sessions. I offer these to help you get new tech skills if you're a writer and learn to express yourself as a writer if you're a programmer. 

Out of respect for the privacy of those taking these sessions, and because I think they're tacky at this point, I'm not asking for testimonials or endorsements. That said, if you'd like to talk with someone who has taken a session, [email me](mailto:gwen@gwenbell.com) and I'll put you in touch with someone who can speak from their experience about how it went.

