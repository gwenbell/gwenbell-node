<img src="/images/gwenbell.jpg" class="profile" style="width: 50%; float: right; margin-left: 1em; margin-bottom: 1em;" >

#### Current

+ Location: Abroad
+ Projects: [#Gaman](/gaman) and Spanish immersion

#### Brief

+ Occupation: Technical writer
+ Current location: Abroad
+ Born: 364739520 UNIX time
+ Car Status: None
+ Life Status: Everything I own fits into 1.5 bags
+ Education: UNC Chapel Hill, University of East Anglia
+ Life Philosophy: Stoic (tranquility trumps all, even fame!)
+ Started career: Teaching English in Kiryu/Gunma, in 2005 opened yoga studio in Yokohama
+ Dream: Build the Gwendolyn Round House [2000](/images/2000-gwendolynroundhouse.jpg) / [2013](/images/2013-gwendolynroundhouse.jpg)
+ Childhood: Before mom's death (I was 11, she was 30), [raised on military bases](/expat). After death, raised by maternal grandparents [Nunu](http://nunubell.com) and Bub
+ Daily Habits: [Appreciations](/appreciations), floss, code, write, edit, walk, write, make bed, clean, look out the window, lengthen spine, practice negative visualizations
+ [Stack](/tech)

#### Writing

[Tech writing](/tech) <br />
[Non-tech writing](/write) <br />
[2007 Bio, courtesy Wayback Machine](http://web.archive.org/web/20071013231526/http://www.gwenbell.com/about)

#### Site

This site is a [Bitters](http://bitters.gwenbell.com) fork committed to [GitLab and Gitboria](http://gwenbell.com/gitnotgithub)

#### Process

"Every day, when I sit down to my work, I start at page one and go through the whole thing, revising freely." - William Gibson [in The Paris Review](http://www.theparisreview.org/interviews/6089/the-art-of-fiction-no-211-william-gibson). 

I consider this site a living repository of ideas. Each day I go through it, revising freely. If there's a piece I've removed that you'd like to access, you might check [my commits](http://gitlab.com/gwenbell/gwenbell).

<div style="font-size: .6em">Last updated 3 December 2013</div>
