### You Are Your Word. Do You Want that Word to be 'Like'?

I'm going to tell you the cheapest, highest impact technology money can buy. For like, free. 

First, I'm going to tell you a story. This story features [Nunu](http://nunubell.com) and [Ev Bogue](http://evbogue.com).

We're visiting, standing in her kitchen. 

She says,

"Gwen honey, can I give you two some feedback?"

I felt pretty nervous, because when my grandmother goes "Gwen, honey," I know she's for real. (And if she calls me Gwendolyn Jane, I know she's pissed. Or, at least that was true growing up.)

So I say, "hmmm....ok!"

"The two of you say something an awful lot. Are you aware of what it is?"

Oh! A puzzle. Will she ding me for my dangling participles?

"You say 'like' and 'y'know' a lot."

"A lot?"

"Every few words," she said.

#### Filler

I hate filler. I work hard to cut the fat, and here I was filling my sentences with two empty words: like and y'know.

The first, although I'm off Fxxxbook. It's where we've gotten 'like'. Yes, it's a word. But when I hear it in converation, and when I _used_ it in conversation, it was an empty calorie. It has zero impact on the listener. It is filler.

I used y'know to encourage the listener to come along with the story I told. It's another way of saying 'you get me? You follow?'

But the pernicious one is the word, 'like'. If you listen to the conversations around you'll hear it in conversation, sometimes as many as every three words.

#### The Solution

Here's the cheap solution. It cured me.

1. Find a pack of playing cards.
1. They must be red.
1. Find the Joker.
1. Each time you or the person with whom you share space uses the word 'like', flag them.
1. Flag yourself. Flash the red joker when you catch yourself using filler.

#### The Harder Solution

Quit Fxxxbook. 

I was already off, and still overused the word 'like'. If you're on there, surrounded by 'like', you'll probably have a harder time. 

Commit to getting off like and cut the filler.

You are your word.

Do you want that word to be 'like'?

<div style="font-size: .6em">Updated 6 December to add piece from 14 December 2010</div>

Why I disabled my FB acct (14 December 2010) today:

Same reason I disabled LinkedIn - too static I don't know what algorithms are feeding me what - no sense of choice. Why I held on? "The kids are doing it." Why I'm quitting (& arguments why I shouldn't) You're a SM expert! No, I'm a social web strategist & don't see how FB fits into a strategy for me. FB as a lowest common denom - it wasn't ever intended for advertisers! Watch The Social Network. Even if effective, it changes overnight. They could change policy.

How they tried to keep me from deactivating. Why I still deactivated.

What does "Like Wealth" even mean? Can you cash it in at the end of the day?

<div style="font-size: .6em">Published 25 November 2013</div>

