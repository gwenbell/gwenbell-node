### OSex Change

Sometime, as I was 'making the switch' from OSX to Linux I said, "I'm happy to be getting off OSX." And then I said, "Oh. Es. Ex. OSex. Fucking Steve Jobs. He built the word sex into his operating system descriptor. WHAT A FUCKING GENIUS!"

Edit that sentence out of your mind if you don't like the f word. Or you don't like the sex word. But those were my actual words and I can't help it if the word sex offends you because you're not getting any.

As I made the switch off OSX and onto Linux something happened inside me. I got more serious about the work. I got more intentional about what goes on my machine. As recent as this week, I switched off Firefox and onto surf.

**As the system, so goes the man**

The deeper I get into the stack, the closer to bare metal, the more I decouple my machine from the task at hand -- _choosing_ the right tool for the job, instead of the default setting. 

The more deliberate I become, the more I demonstrate I _care_.

I've been accused of the opposite. After I wrote a damning piece about the destructive qualities of BaceFook, I got a message from a reader. The essence was, Oh look at you. You're playing with the boys now and you're going to show us your big swinging dick. 

The person actually said that: big swinging dick.

I remember feeling amused. What? Ok, if that's how you took that. 

I do think it's a rare man in tech with balls these days, but I'm not trying to switch over to dudehood. 

I have for years preferred the company of men, they almost never interrupt me when I'm working. I get more work done on the command line and my brain does seem to be moving in a more male direction, if such a thing can be said. 

The tools I use are MAN tools, IF that's what a Linux setup is. 

There are no reassuring smiley faces in my dock. I don't have a dock. 

Nor a dick.

It fascinates me that we equate, as a culture, bravery as "having balls." I won't get too deep into sexist language other than to say if you have to think of me as a man in order to grok my work now that I'm this deep in the stack, then. Do.

Still, something shoots through this work, same as it always did. What we equate in this culture with a feminine desire: the desire to share with others. The desire to be present with people, the desire to hear their stories, the desire to teach them there's a way past OSX.

When [you're ready, I'm ready](/pairprogram).

Companion piece: [There's No Ghost in My Machine](/revolution).

