### Sent From The Command Line

<img src="public/images/mf-terminal-love-letter.jpg" class="profile" size="100%">

Something I hate so much, but has finally, mercifully slowed: receiving 'sent from my iPenis' messages. 

I hate getting a message that says, 'sent from my iPxx' or 'sent from my iPhxxx.' I just lump them all together into 'sent from my iPenis' and pretend it didn't happen.

As of last night, I'm getting the best revenge! I can and for a while will until it gets old -- sign my messages 'sent from my command line,' because I got Mutt, [The Homely Mail Client](http://stevelosh.com/blog/2012/10/the-homely-mutt/) going last night. I saw Mutt ages ago but wasn't frustrated enough with Thunderbird yet to get it going. I hit that point earlier this year. 

Now, thanks to Mutt, I

	getmail

If you'd like to look at the documentation, [getmail](http://pyropus.ca/software/getmail/). Written in Python.

Then I

	mutt

And it opens to my messages. 

Then I vim in to respond, hit send and it all happens from mutt to vim to mutt again. Then I'm out.

#### Deliberate Tech

Do you hear what I'm saying here? Technology the way I want it - on the receiving and sending side - is DELIBERATE. Not slower tech. Not less tech. Not even minimalist, though yes, I like things lean. I want, and therefore make, my digital environment in a [deliberate](/like) way. Now you know when you get an email from me I didn't just 'pop it off' from my iPenis on my way to catch the next train.

It's possible to be both intentional and effective.

How?

Be deliberate.

There's something to it, isn't there? Being able to say: 'Sent from the command line.'

<div style="font-size: .6em">Updated 26 November 2013</div>

