### The ideal publishing platform. (For at least one person.)

[Bitters](http://bitters.gwenbell.com) is a lightweight publishing platform. It's what I've built and used for the past six months.

It's designed for writers who love to write and are willing to do a little programming to get the site they want.

I say little. Really, once you get the urge to be on the command line, a little may fast turn into a lot. YOU HAVE BEEN FOREWARNED.

This is how Bitters works.

**1** You first get your mitts on a VPS. I wrote about VPSs in [ten tech tools](/tentools). I use [Digital Ocean](https://www.digitalocean.com/?refcode=14a7846491d4) [rewards link], but there are others, like Linode, though Digital Ocean seems to do the trick and it's cheap/reliable by most standards.

You can also share a hosting plan at first if you want to (I did). It's similar to sharing a drawer with a buddy. Good temporary solution, but long term you'll probably want your own so you can break everything and no big deal!

**2** You will want to clone Bitters down. To do that you'll need to know at least a smidgen of Git.

**3** When you clone it to your local machine you're going to see several files that are key to making Bitters work.

In app.js you have your module dependencies. Also, routes, as you create new ones, will go in here.

This is an easy step to forget when you're creating routes, especially if you don't know what a route is.

**4** Basic structure of an Express project includes routes. Routes operate in a simliar way to how the post office gets the postcard to your grandmother. When you write her zip code on it, you're routing it to her town, in her city, in her state. 

When you use routes in Express, you're telling the server where to go to retrieve the file you've just created.

Ok!

**5** Then you're going to see another file, index.js. This is where you get to learn some basic JavaScript. If you want a title, you require it. The way you do that is call

	exports.index = function(req, res){
	  res.render('index', { title: 'Gwen Bell | Title Page'});
	};

What that'll do is pull the title onto the top of the index page. For other pages, you'll require them once you've created them.

**6** Then you've got a Views folder. In there you have the magic that is Jade. Jade is one of TJ Holowaychuk's brainchildren. So when you have a new piece, say, a piece on Arch, you do

	extends layout

and that pulls the layout in and ensures the same layout across the whole site (you might remember a trick in php that did that in the prehistoric days when you used WordPress, before it became a BotNet).

	block content
	  include ../arch.md

That's what makes the markdown file display.

Markdown! That is its own thing, and it's amazing and if you're interested in learning, [holler](mailto:gwen@gwenbell.com). 

**RECAP** 

Bitters: VPS, shared or solo flight. Git bare minimum, the ability to clone/push/pull. 

Express, Jade...and Markdown, which I can just say to you, if you're a writer and want to come into programming... [gaman](/gaman) and learn it.

<div style="font-size: .6em">Updated 24 November 2013</div>
