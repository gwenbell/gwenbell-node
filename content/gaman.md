## GAMAN

> Gaman is a Japanese term that means perservering despite difficulty. When life presents you with a challenge, you dig deep and find that gaman spirit to move forward.

I've created a private repo for this year's end of year project. I put the repo on GitLab with all 31 prompts in it.

Pop into the [IRC channel](/irc) and let me know your username for access to the repo.

For every person who signed up for Gaman, I got ten emails asking for an easier way through. Many of you said you don't want to learn Git. Those who were this close to committing told me you don't want to publish your writing to the public web. This rest of you, from what I can tell, want to avoid doing hard work. The whole point of gaman is to do difficult things.

Maybe it'll help you to understand why Git matters. Git matters because it keeps your work cryptographically secure. That means your repo, the place you put your writing, can't be tampered with by anyone who doesn't have the private key.

Speaking of private keys!

#### Privacy

The focus in tech now is privacy. We're post-NSA rolling revelation jubilee, so, to be specific the focus is privacy _breaches_.

You're [not the only one who doesn't trust America with your data anymore](http://www.bloomberg.com/news/2013-11-26/nsa-spying-risks-35-billion-in-u-s-technology-sales.html). I'm rolling with it, and doing a private repo and adding those of you who want to access the prompts to the private repo. On [30 November 2013](/momday), the top story on HN was [In God We Trust, Maybe, But Not Each Other](http://apne.ws/1dHFLRt).

Before you visit #gaman on IRC, set up a private repo on GitLab, and add me as a dev. I'll ship you all the Gaman prompts in one go. You can run through them on your own, or share your responses with others as you see fit.

If you decide you want to respond to the prompts in public, do so, and then share your response in IRC. But you've got to get a handle on Git to get the prompts.

Gaman, <br />
Gwen <br />
1 December 2013 </br>

***
[The End Result](#result) | [Why Gitlab?](#gitlab) | [Markdown](#markdown) | [Branch](#branch) | [Branch workflow](#branching)| [Wiki](#wiki) | [Why Bother?](#bother) | [GSA](#gsa)

### Gaman Day 7

#### The End Result <a id = "result"></a>

> < mil3s> gwenbell, what is the end result in the Gaman course?

mil3s, thanks for asking. The thing about getting up each day, writing something down, being honest with yourself and the people reading what you've written, figuring out how to branch and merge, learning to use Git by doing it. 

The thing about all that is, it's actually painful. There's a lot of suffering built into gaman. It's hard work. It's not publishing a status update to a social network.

I enjoy doing hard things. (In 2007, publishing a status update to a social network was kinda hard, believe it or not. It's obvious it's not hard anymore.)

I think each time I do a hard thing, even a tiny hard thing, my human experience improves. Each time I wear a little less clothing than is required and 'suck it up' rather than complaining, I become a more resilient person. 

I believe it could always be worse, and that's the spirit of gaman. It could always be worse. When you reframe your life, your day, yourself as "it could always be worse" rather than "this is the worse thing that could ever have happened _ever_" a few things start to occur.

1. You give up on your drama/drop the storyline
1. You take responsibility for yourself
1. You learn some new skills
1. You become more valuable to other people
1. You grow

The point of gaman isn't to "get" to some state out there. There is no "there" -- we are each on an infinite cycle that is ourselves, right up until our last breath. 

The point of gaman, the point of the work I do, the work I offer, is to show you ways to reconcile yourself to yourself while doing hard things -- committing to showing up to the command line to do the work each day. Or, you can do the easy thing. And not grow. Your choice.

### Gaman Day 6

#### Why Bother? <a id = "bother"></a>

>I don't know why we are here, but I'm pretty sure that it is not in order to enjoy ourselves. - Ludwig Wittgenstein

### Gaman Day 5

#### Git Status Announcement <a id = "gsa"></a>

Does everyone know that to get the latest version of everyone's markdown file, you do a git pull origin master each morning/evening when you check in?

I'm merging your markdown files to master, you've got to do a daily pull to get the latest from your fellow writers, as well as to see your own update files. If you don't do a merge request, you won't see your own markdown file updated. 

### Gaman Day 4

#### The Wiki <a id = "wiki"></a>

Gaman now has [a wiki!](https://gitlab.com/gwenbell/gaman/wikis/home)

### Gaman Day 3

#### Branch Workflow <a id = "branch"></a>

Lots of questions today about branching. So let's go from the top.

Branch off master. Don't branch off someone's repo.

	git checkout -b yourinitials

Work on your response to the day's prompt in yourinitials.md. Don't delete anything.

Git add and git commit your changes to your markdown file.

	git push origin yourinitials

Create a merge request and I'll merge it.

Holler if you still need help. We're in IRC all day/all night. Except when I'm eating, sleeping or peeing.

### Gaman Day 2 <a id = "2"></a>

Day two of Gaman I saw a lot of two things. I want to celebrate both! Branching and editing your markdown.

#### Branch <a id = "branch"></a>

Let's start with what I see as the harder of the two. 

Branching in Git. The purpose of the branch is for you to work on your own piece of the puzzle before integrating with master. If this were a code project, you'd have your commit checked by another human or multiple humans. That way you don't insert something that breaks an entire code base.  

To branch, you

	git checkout -b yourbranchname

We're going with the convention of first initial, last initial, so I do

	git checkout -b gb

Thing is, we're not doing anything on the production side. You branch and if you're Linus Torvalds, you'd do 

	git checkout -b lt

Then  work on your own branch. Once done, add and commit your markdown file. 

	git add lt.md

Once you're ready to have it merged to master, do a merge request. 

Someone with the power to do a merge to master -- yours truly -- will merge your branch once I check it. If you don't want to be merged to master because you're working on your own thing for gaman, simply fork. No need to branch.

#### Markdown <a id = "markdown"></a>

Saw markdown edits yesterday. I haven't put out a style guide, but if you want to know what I do:

	## GAMAN

for page headers

	### Gaman Day 2

for sub-heads

	#### Branch

for head 3. I know that's a lot, but it breaks the page up in a way I find pleasant.

As for links, 

	[Name of page](http://locationofpage.com)

More questions? Holler in [#gaman on IRC](/irc).

Great work yesterday. Exciting to see your commits, your branches and your markdown edits. And it was only day 2! 

Bonus: [Git Aliases](http://tjholowaychuk.com/post/26904939933/git-extras-introduction-screencast)

> It was fun yesterday to finally see it happening after being utterly confused for a while. - Hannah van der Deijl, On Git and Gaman

### Gaman Day 1 <a id = "1"></a>

After the first day of Gaman, the biggest question in the [IRC](/irc) channel and in my inbox was:

**What is SSH and how and why do I generate a private key?** <a id = "ssh"></a>

Every morning after I do my practice, make coffee and write, I ssh into my [remote server - an Arch instance on Digital Ocean](https://www.digitalocean.com/?refcode=14a7846491d4
) (referral code), so it's natural for me at this point. But! There was a time when I didn't understand public-key cryptography at all. If you need more guidance than what I'm writing here, Digital Ocean has a [guide to SSH](https://www.digitalocean.com/community/articles/how-to-set-up-ssh-keys--2) with code samples.

#### On Windows

If you're on a Windows machine, [read this guide](http://guides.beanstalkapp.com/version-control/git-on-windows.html). I read through it and it's thorough -- let me know if you've found a better tutorial. Also, you might try installing [Arch](/arch).


#### What, Why and How

SSH means Secure SHell. Here's [the why](https://en.wikipedia.org/wiki/Secure_Shell), "The encryption used by SSH is intended to provide confidentiality and integrity of data over an unsecured network, such as the Internet." 

To get going, click on your profile in [GitLab](http://gitlab.com) and you'll see 'SSH Keys' as an option at the top - fourth from home. Click on 'add SSH key'. Follow the instructions provided. Once you've generated your key pair, you copy and paste it into the SSH Key window and give it a name. I call mine Local Laptop and Arch. 

#### Why GitLab? <a href="gitlab"></a>

A second question I've gotten is "why not just use the other mainstream website for Git?" The answer is, I [prefer GitLab](/gitnotgithub) and I'm using it to manage Gaman. If you spend a few minutes with it, you might, too. If you don't, no big deal!

That was Gaman Day 1. What will Day 2 bring!?

<div style="font-size: .6em">Updated 5 December 2013</div>

