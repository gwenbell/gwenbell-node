### Flourish

This afternoon I spent half the day with a five year old. I was inspired to be in her presence.

She wanted me to paint her nails the same color as mine, this deep rouge. She actually called it that, 'rouge, a francais,' and as English is her third language I asked her what words do you know?

"What" she said.

I said, yep, that's one of the wh words! Her dad goes, "what are the wh words?"

And I said they are who, what, when, where, why and how! They are the words you have to use to make a good story. Who was there, what happened, where were you, and how'd that go. Also. Why'd it happen! That's how you use the wh words.

And then I asked her later on what other words she knows, and she said, "I know..."

Later on, she spoke the other word she knows in English.

Flourish.

I couldn't beleive it. How'd you learn that word, I asked. From your dad?

Her dad said, "what does that word mean, really?"

I said, well, you add a final flourish to a painting or a song. If you water a tree or plant, and give it light, it flourishes. It thrives.

So there are two meanings.

"Flower," he said, and I said, "I suppose that is the root, isn't it? Flour --- fleur --- flower --- flourish."

So I am out of the country. And I am **in** in this one.

And. I am flourishing.

<div style="font-size: .6em">Published 8 December 2013, edited down from gaman response</div>

