### TMUX

Ok! Number one question right now. What is tmux? How do I use it? And...more to the point...why would I?

Let's take this one question at a time.

**What is tmux?**

Tmux means terminal multiplexer. I think that's such a funny name. It sounds so ... magnificent! When really all it's doing is monitoring your irc room when you're not in it.

**Why would I tmux**

You would tmux if you'd like to go to sleep and have the room/channel you're in be available to read once you've awoken. So for #gaman, I attach tmux, fall alseep, and wake up, and can add names to the [gaman participant list](/gaman). No problem. Without tmux, I have no idea you visited the room, nor what you said. With tmux, I see what you said, when you said it. Magic or what?

**How do I tmux?**

First, you gotta have a computer that never sleeps. Mine is an [Arch](/arch) instance on [Digital Ocean](https://www.digitalocean.com/?refcode=14a7846491d4). That's a rewards link.

Now that you've got your instance up -- [read this if you need help](http://arch.evbogue.com) -- you can set up tmux.

If you're on an Arch instance, it's as simple as

	$ sudo pacman -S tmux

Then, to start a tmux instance when you login, do

	$ tmux

Then start irssi within tmux

	$ irssi

Now is your chance to log into your favorite IRC channels in Irssi

Then, to detach, you do

	$ ctrl-b

Then

	d

Which detaches you. Now, you're using tmux, your instance will stay awake even as you go to sleep, and when you wake up in the morning you do 

	$ tmux attach

You'll see what you missed while you slept. Repeat the process when you want to detach again -- if you don't detach that way, nothing will be there when you return.

Questions? [Come into](/irc) #gaman and ask -- someone will help you, probably.
